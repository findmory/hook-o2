'''
slicebot hook module for O2

authors: Mike, Ory, Tom Quinn, Jason Skrifvars

version: 2016.10.26

- Handles the post processing for O2 customers to link their upLynk assets to o2 media objects
'''

import base64
import urllib
import urllib2
import zlib
import hmac
import hashlib
import time
import json
import os
import sys
from xml.dom.minidom import parse
from collections import OrderedDict


# O2 account info
O2_COMPANY_KEY = '<O2_COMPANY_KEY>'
O2_API_URL = 'http://api.vidible.tv/{0}/video'.format(O2_COMPANY_KEY)

# UPLYNK account info
UPLYNK_OWNER = '<UPLYNK_OWNER_ID>'
UPLYNK_SECRET = '<UPLYNK_API_SECRET>'
UPLYNK_ROOT_URL = 'http://services.uplynk.com/api2'

def log(*args):
    try:
        line =  time.strftime('%Y-%m-%d %H:%M:%S  ') + ' '.join(str(x) for x in args) + '\n'
        sys.stderr.write(line)
        sys.stderr.flush()
    except:
        print 'log:',

def OnJobComplete(cfg, job):
    '''
    called when slicebot finishes slicing and encoding
    '''
    id = job.asset_id
    log("job ID = {}".format(id))
    asset = get_asset_by_id(id)
    create_o2_media(asset)



def get_asset_by_id(id):
    retries = 6 # actual number of retries is retries - 1, so 5 here, you can change it if you want to increase retries
    backoff_times = [2**c -1 for c in range(retries)]
    for index, backoff_time in enumerate(backoff_times):
        # backoff for backoff_time seconds
        time.sleep(backoff_time)
        asset = call_uplynk('/asset/get', id=id).get('asset')
        if asset is None and index < retries - 1:
            print('Failed retrieving asset {0} time(s).. Retrying in {1} seconds'.format(index+1, backoff_times[index+1]))
        else:
            break

    log('*** GET ASSET RESPONSE FROM UPLYNK ***')
    log(asset)
    return asset


def get_assetinfo_by_assetid(asset_id):

    assetinfourl = "http://content.uplynk.com/player/assetinfo/{0}.json".format(asset_id)
    #  call asset info API
    req = urllib2.Request(assetinfourl)
    data = json.load(urllib2.urlopen(req))
    log('*** GET ASSET INFO FROM UPLYNK ***')
    log(data)
    return data


def build_o2_request(request_url):
    '''
    build the web request and append the token for a o2 API call
    :param request_url: url of the O2 service you will be calling
    :return: skeleton web request with auth token
    '''
    request = urllib2.Request(request_url)
    request.add_header('Content-Type', 'application/json')
    return request


def create_o2_media(asset):
    '''
    creates a Media Object in o2
    :param asset: uplynk asset
    :return:
    '''
    log('sending POST request to O2')
    log('asset is {}'.format(asset))
    request = build_o2_request(O2_API_URL)
    title = asset.get('desc')
    uplynkAssetId = asset.get('asset') if asset.get('asset') else asset.get('id') # when asset is returned from slicebot job this is "asset" otherwise it's "id"
    upLynkVideoURL = 'http://content.uplynk.com/{0}.m3u8'.format(uplynkAssetId)
    thumblist = get_uplynk_thumbs(asset)
    body = dict(
      {
        "name": title,
        "storageType:": "EXTERNAL",
        "encodedVariants": [
          {
            "advancedQuality": False,
            "aspectRatio": 0,
            "audioBitRate": 96,
            "audioQuality": 5,
            "crop": False,
            "fileSizeBytes": 7305771,
            "frameStep": True,
            "height": 250,
            "mono": True,
            "provider": "UPLYNK",
            "cdnProvider": "UPLYNK",
            "videoBitRate": 1500,
            "videoQuality": 3,
            "videoUrl": upLynkVideoURL,
            "width": 300,
            "tokenEnabled": True,
          }
        ],
        "thumbnails": [
          {
            "chosenType": "AUTO",
            "covering": True,
            "type": "AUTO",
            "sizes": thumblist,
            "version": 1
          }
        ],
        "vlsId":uplynkAssetId
      })

    request.data = json.dumps(OrderedDict(sorted(body.items(), key=lambda t: t[0])))
    log("o2 Request Body: ", request.data)


    # try writing to O2 a few times, with delay
    # Retry this many times before giving up.
    def update_o2():
        try:
            resp = json.loads(urllib2.urlopen(request).read())
            log('asset: {} success'.format(title))
            return resp
        except:
            log('asset: {} retrying O2 write'.format(title))
            return 0

    TRIES = 3
    _resp = None
    for i in range(0, TRIES):
        _resp = update_o2()
        if _resp != 0:
            break
        else:
            time.sleep(1)

    if _resp:
        log('*** DONE PROCESSING {0}'.format(_resp['id']))
    else:
        log('[ERROR] THERE WAS AN ERROR WITH THE O2 API.  Asset {} not written to O2 ***'.format(title))

    return _resp

def get_uplynk_thumbs(asset):
    '''
    Builds the uplynk thumbnail list for this asset
    :param asset:
    :return:
    '''
    log('*** GENERATING SMALL & LARGE THUMBNAIL ***')
    asset_id = asset.get('asset') if asset.get('asset') else asset.get('id')
    asset_info = get_assetinfo_by_assetid(asset_id)
    thumb_prefix = asset_info.get('thumb_prefix')
    max_slice = asset_info.get('max_slice')

    thumbs = []

    for n in range(0, max_slice + 1):
        if n == 1:
            break
        str(n)
        filename_small = thumb_prefix + "{0:08x}".format(n) + '.jpg'

        small = {
                  "url": filename_small,
                  "width": 128,
                  "height": 96,
                  "original": False
                }

        log('*** Add Small Thumb to list ***')
        thumbs.append(small)

        filename_large = thumb_prefix + 'upl256' + "{0:08x}".format(n) + '.jpg'
        large = {
                  "url": filename_large,
                  "width": 256,
                  "height": 192,
                  "original": False
                }

        log('*** Add Large Thumb to list ***')
        thumbs.append(large)

    return thumbs


def call_uplynk(uri, **msg):
    '''
    base method for uplynk API calls
    :return: uplynk API response
    '''
    msg['_owner'] = UPLYNK_OWNER
    msg['_timestamp'] = int(time.time())
    msg = json.dumps(msg)
    msg = zlib.compress(msg, 9).encode('base64').strip()
    sig = hmac.new(UPLYNK_SECRET, msg, hashlib.sha256).hexdigest()
    body = urllib.urlencode(dict(msg=msg, sig=sig))
    resp = json.loads(urllib2.urlopen(UPLYNK_ROOT_URL + uri, body).read())
    return resp

