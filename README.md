### SETUP

Place the script `hook_o2.py` in the same folder as your slicebot app

Replace these three lines of the script with your correct information
```python
O2_COMPANY_KEY = '<O2_COMPANY_KEY>'
UPLYNK_OWNER = '<UPLYNK_OWNER_ID>'
UPLYNK_SECRET = '<UPLYNK_API_SECRET>'
```
Edit your slicebot.cfg file to include this:
```
[global]
hook_modules = hook_o2
description = [FILENAME_SHORT]   # [FILENAME_SHORT] is the literal value here, this is not for substitution
```

Start the slicebot with `./slicebot path/to/slicebot.cfg`

Profit

